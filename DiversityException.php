<?php

namespace TBureck\Diversity\Library\Common;

/**
 * Class DiversityException
 * @package TBureck\Diversity\Library\Common
 *
 * @author Tim Bureck
 * @since 2016-02-29
 */
class DiversityException extends \Exception
{

    protected $externalMessage = '';

    public function __construct($message = '', $code = 0, $previous = null, $externalMessage = '')
    {
        parent::__construct($message, $code, $previous);

        $this->externalMessage = $externalMessage;
    }

    /**
     * @return string the external message to be printed out to the user of the application.
     */
    public function getExternalMessage()
    {
        return $this->externalMessage;
    }
}