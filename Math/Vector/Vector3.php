<?php

namespace TBureck\Diversity\Library\Common\Math\Vector;

/**
 * This class resembles a vector with 3 dimensions.
 *
 * @package TBureck\Diversity\Library\Common\Math\Vector
 *
 * @author Tim Bureck
 * @since 2016-02-24
 */
class Vector3
{

    /**
     * @var number
     */
    protected $x;

    /**
     * @var number
     */
    protected $y;

    /**
     * @var number
     */
    protected $z;

    public function __construct($x = 0, $y = 0, $z = 0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
    }

    #region Basic arithmetic
    /**
     * Adds an other vector to this vector.
     *
     * @param Vector3 $that the vector to add to this vector
     */
    public function add(Vector3 $that)
    {
        $this->x += $that->x;
        $this->y += $that->y;
        $this->z += $that->z;
    }

    /**
     * Subtracts an other vector from this vector.
     *
     * @param Vector3 $that the vector to subtract from this vector
     */
    public function subtract(Vector3 $that)
    {
        $this->x -= $that->x;
        $this->y -= $that->y;
        $this->z -= $that->z;
    }

    /**
     * Scales this vector by the given factor
     *
     * @param number $factor the factor to scale the vector with
     */
    public function scale($factor)
    {
        $this->x *= $factor;
        $this->y *= $factor;
        $this->z *= $factor;
    }
    #endregion

    #region Geometrical meanings
    /**
     * @return float
     */
    public function length()
    {
        return sqrt(pow($this->x, 2) + pow($this->y, 2) + pow($this->z, 2));
    }
    #endregion

    #region Other operations
    /**
     * @return Vector3 returns a vector with the same direction of length 1
     */
    public function normalize()
    {
        $length = $this->length();

        return new Vector3($this->x / $length, $this->y / $length, $this->z / $length);
    }
    #endregion

    /**
     * @return number
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return number
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return number
     */
    public function getZ()
    {
        return $this->z;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%f, %f, %f', $this->x, $this->y, $this->z);
    }
}