<?php

namespace TBureck\Diversity\Library\Common\Math\Vector;

/**
 * This class resembles a vector with 2 dimensions. It's implemented by extending the Vector3 class and ignoring its Z
 * value. The method getZ() is overridden to always return 0.
 *
 * @package TBureck\Diversity\Library\Common\Math\Vector
 *
 * @author Tim Bureck
 * @since 2016-02-24
 */
class Vector2 extends Vector3
{

    /**
     * Vector2 constructor.
     *
     * @param number|int $x
     * @param number|int $y
     */
    public function __construct($x = 0, $y = 0)
    {
        parent::__construct($x, $y, 0);
    }

    #region Other operations
    /**
     * @return Vector2 returns a vector with the same direction of length 1
     */
    public function normalize()
    {
        $length = $this->length();

        return new Vector2($this->x / $length, $this->y / $length);
    }
    #endregion

    public function getZ()
    {
        return 0;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%f, %f', $this->x, $this->y);
    }
}