<?php

namespace TBureck\Diversity\Library\Common\Math;

/**
 * This class provides functionality to check, whether a number is between a given range, or modify to ensure that it is.
 * 
 * @package TBureck\Diversity\Library\Common\Math
 *
 * @author Tim Bureck
 * @since 2016-02-24
 */
class Clamp
{

    /**
     * Checks, whether the given $value is between $min and $max. By default, $min and $max are considered between, if
     * $value is equal to $min or $max respectively. This can be changed using the $minIncluding and $maxIncluding
     * parameters.
     *
     * @param float|int $value
     * @param float|int $min
     * @param float|int $max
     * @param bool  $minIncluding
     * @param bool  $maxIncluding
     *
     * @return bool true, if the given $value is between $min and $max
     */
    public static function between($value, $min, $max, $minIncluding = true, $maxIncluding = true)
    {
        if (!$minIncluding && !$maxIncluding) {
            return $value > $min && $value < $max;
        } else if ($minIncluding && !$maxIncluding) {
            return $value >= $min && $value < $max;
        } else if (!$minIncluding && $maxIncluding) {
            return $value > $min && $value <= $max;
        } else {
            return $value >= $min && $value <= $max;
        }
    }

    /**
     * Clamps the given $value between $min and $max.
     *
     * @param float|int $value
     * @param float|int $min
     * @param float|int $max
     *
     * @return float the value, if it is greater or equal min and lower than or equal max; min, if value is lower than
     * min or max, if value is greater than max.
     */
    public static function clamp($value, $min, $max)
    {
        return max(min($value, $max), $min);
    }
}