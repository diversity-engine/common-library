<?php

namespace TBureck\Diversity\Library\Common\Math\Random;

/**
 * This class aims at generating random numbers that are normally distributed. More info about normal distribuation can
 * be found on Wikipedia:
 *
 * https://en.wikipedia.org/wiki/Normal_distribution
 * https://en.wikipedia.org/wiki/Normal_distribution#Generating_values_from_normal_distribution
 * https://en.wikipedia.org/wiki/Marsaglia_polar_method
 *
 * @package TBureck\Diversity\Library\Common\Math\Random
 *
 * @author Tim Bureck
 * @since 2016-02-24
 */
class NormalDistribution
{

    /**
     * Generates a normally distributed random number using the polar method.
     *
     * @param int $mean
     * @param int $deviation
     *
     * @return float
     */
    public function polar($mean = 0, $deviation = 1)
    {
        do {
            $u = 2 * (mt_rand() / mt_getrandmax()) - 1;
            $v = 2 * (mt_rand() / mt_getrandmax()) - 1;
            $q = $u * $u + $v * $v;
        } while ($q == 0 || $q >= 1);

        $p = sqrt(-2 * log($q) / $q);

        return $deviation * $u * $p + $mean;
    }
}