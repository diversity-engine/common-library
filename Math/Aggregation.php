<?php

namespace TBureck\Diversity\Library\Common\Math;

/**
 * This class provides aggregation functionality, like sums or average values.
 *
 * @package TBureck\Diversity\Library\Common\Math
 *
 * @author Tim Bureck
 * @since 2016-02-24
 */
class Aggregation
{

    /**
     * Calculates the average value of all number values in the given array.
     *
     * @param array|number[] $values
     *
     * @return number the average value of all values in the given array or 0, if the array is empty.
     */
    public static function average(array $values)
    {
        return ($count = count($values))
            ? self::sum($values) / $count
            : 0
        ;
    }

    /**
     * Calculates the sum of all values in the given array.
     *
     * @param array|number[] $values the values to be summed up.
     *
     * @return number the sum of the given values.
     */
    public static function sum(array $values)
    {
        return array_sum($values);
    }
}